
public class Room2 {// new class
	public static String wallcolor; //initializing 
	public static Integer window;
	public static String floor; 
	
	public static void main (String[] args) { //static means public not private
		Room room1 = new Room("Yellow", 1, "Hard Wood"); //room one
		System.out.println("Room 1: " + room1.getWallcolor() + "," + room1.getWindow() + "," + room1.getFloor());
		Room room2 = new Room("Purple", 0, "Tiled"); // room two
		System.out.println("Room 2: " + room2.getWallcolor() + "," + room2.getWindow() + "," + room2.getFloor());
		Room room3 = new Room("White", 3, "Carpeted"); // room three
		System.out.println("Room 3: " + room3.getWallcolor() + "," + room3.getWindow() + "," + room3.getFloor());

	}
}