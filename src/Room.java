
public class Room  {
	private String wallcolor; //initializing
	private Integer window;
	private String floor;
	
	public Room () { // default constructor
		wallcolor = "No wallcolor";
		window = 0;
		floor = "No floor";
	}
	public Room (String wallcolor, int window, String floor) {
		this.wallcolor = wallcolor;
		this.window = window;
		this.floor = floor;
	}
	// set methods
	public void setWallcolor (String wallcolor) {
		wallcolor = this.wallcolor; //this. specifies the wall color
	}
	public void setWindow(Integer window) {
		window = this.window;
	}
	public void setFloor(String floor) {
		floor = this.floor;
	}
	// get methods
	public String  getWallcolor() {
		return this.wallcolor;
	}
	public Integer getWindow() {
		return this.window;
	}
	public String getFloor() {
		return this.floor;
	}
	
}

